<?php

use Doctrine\DBAL\Schema\Table;

$schema = $app['db']->getSchemaManager();

if (!$schema->tablesExist('user')) {
    $users = new Table('user');
    $users->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
    $users->setPrimaryKey(array('id'));
    $users->addColumn('firstName', 'string', array('length' => 50));
    $users->addColumn('lastName', 'string', array('length' => 50));
    $users->addColumn('username', 'string', array('length' => 50));
    $users->addUniqueIndex(array('username'));
    $users->addColumn('password', 'string', array('length' => 255));
    $users->addColumn('salt', 'string', array('length' => 255));
    $users->addColumn('accessToken', 'string', array('length' => 255));
    $users->addColumn('email', 'string', array('length' => 50));

    $schema->createTable($users);
}

//if (!$schema->tablesExist('folder')) {
//    $folders = new Table('folder');
//    $folders->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
//    $folders->setPrimaryKey(array('id'));
//    $folders->addColumn('name', 'string', array('length' => 255));
//    $folders->addColumn('created', 'datetime');
//
//    $schema->createTable($folders);
//
//    $app['db']->insert('folder', array(
//        'name' => NO_FOLDER,
//        'created' => date("Y-m-d G:i:s")
//    ));
//}
//
if (!$schema->tablesExist('note')) {
    $notes = new Table('note');
    $notes->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
    $notes->addColumn('user_id', 'integer', array('unsigned' => true));
//    $files->addColumn('folder_id', 'integer', array('unsigned' => true));
    $notes->setPrimaryKey(array('id'));
    $notes->addColumn('title', 'string', array('length' => 255));
    $notes->addColumn('content', 'string', array('length' => 255));
    $notes->addColumn('created', 'datetime');
    $notes->addForeignKeyConstraint('user', array('user_id'), array('id'), array('delete' => 'cascade'));
//    $files->addForeignKeyConstraint('folder', array('folder_id'), array('id'), array('delete' => 'cascade'));

    $schema->createTable($notes);
}


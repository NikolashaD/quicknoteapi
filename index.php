<?php
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/vendor/sphinx/api/sphinxapi.php';
use Symfony\Component\HttpFoundation\Response;


$app = new Silex\Application();
$app['debug'] = true;

require_once __DIR__.'/database/config.php';
require_once __DIR__.'/database/create.php';
require_once __DIR__.'/app/config.php';


$app->run();
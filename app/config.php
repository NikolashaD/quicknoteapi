<?php


define ('SECRET_API_TOKEN',  '2718281828459045');
$app->mount('/api', new \ServiceBundle\Controller\RoutesController());


$app->register(new Silex\Provider\SessionServiceProvider());

$app->before(function ($request) {
    $request->getSession()->start();
});


$app->register(new Silex\Provider\UrlGeneratorServiceProvider());


$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'api' => array('pattern' => '^/api'),
        'default' => array(
            'pattern' => '^.*$',
            'anonymous' => true,
        ),
    ),
    'security.access_rules' => array(
        array('^/.+$', 'ROLE_USER'),
    )
));

function getBaseUrl(){
    return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http' .'://'. $_SERVER['SERVER_NAME'];
}






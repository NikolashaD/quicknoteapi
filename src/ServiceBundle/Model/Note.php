<?php

namespace ServiceBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\DBAL\Connection;
use Silex\Application;

class Note
{
    private $title;
    private $content;
    private $created;
    
    private $user;
    private $folder;


    private $conn;
    private $session;

    public function __construct(Application $app, $title = '', $content = '')
    {
        $this->conn = $app['db'];
        $this->session = $app['session'];
        
        $this->title = $title;
        $this->content = $content;
    }

    public function createNote($userId = null, $folder = null)
    {  
        $this->user = $userId;
        $this->folder = $folder;
        self::saveNote();   
    }

    private function saveNote()
    {
        $this->conn->insert('note', array(
            'title' => $this->title,
            'content' => $this->content,
            'created' => date("Y-m-d G:i:s"),
            'user_id' => $this->user,
//            'folder_id' => $this->folder
        ));
    }

    public function getNotesByUser($user = null)
    {
        $sql = 'SELECT n.id, n.title, n.content, n.created FROM note n LEFT JOIN user u ON n.user_id = u.id WHERE n.user_id = :user ORDER BY created DESC';
        $data = array('user' => $user);
        $notes = $this->conn->fetchAll($sql , $data);

        return $notes;
    }

    public function getNotesByFolder($folderId = null)
    {
        $sql = 'SELECT n.id, n.name, n.created, u.username, u.id as userid, fo.name as foldername FROM note n LEFT JOIN user u ON n.user_id = u.id LEFT JOIN folder fo ON n.folder_id = fo.id OR n.folder_id IS NULL WHERE n.folder_id = :folderId GROUP BY n.id';
        $data = array('folderId' => $folderId);
        $notes = $this->conn->fetchAll($sql , $data);

        return $notes;
    }

    public function getNoteById($id)
    {
        $sql = 'SELECT * FROM note WHERE id = :noteId';
        $data = array('noteId' => $id);
        $note = $this->conn->fetchAssoc($sql , $data);

        return $note;
    }

    public function deleteNote($noteId)
    {
        $sql = 'DELETE FROM note WHERE id = '.$noteId.'';
        $query = $this->conn->exec($sql);
    }
}
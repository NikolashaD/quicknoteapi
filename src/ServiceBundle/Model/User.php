<?php

namespace ServiceBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\DBAL\Connection;
use Silex\Application;

class User
{
    private $firstName;
    private $lastName;
    private $username;
    private $password;
    private $salt;
    private $accessToken;
    private $email;

    private $conn;

    public function __construct(Application $app)
    {
        $this->conn = $app['db'];
    }

    public function addUser($firstName, $lastName, $username = '', $password = '', $email = '', $accessToken = '')
    {
        $salt = self::generateRandomString(20);
        $hash = md5($salt . $password);

        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->username = $username;
        $this->password = $hash;
        $this->salt = $salt;
        $this->accessToken = $accessToken;
        $this->email = $email;

        self::saveUser();
    }

    public function updateUser($userId, $fields = array('id' => '', 'firstName', 'lastName', 'username' => '', 'password' => '',  'email' => '','accessToken' => ''))
    {
        $select = 'SELECT * FROM user WHERE id = '.$userId;
        $user = $users = $this->conn->fetchAssoc($select);

        $salt = self::generateRandomString(20);

        if(isset($fields['firstName'])){
            $this->firstName = $fields['firstName'];
        }else{
            $this->firstName = $user['firstName'];
        }      
        
        if(isset($fields['lastName'])){
            $this->lastName = $fields['lastName'];
        }else{
            $this->lastName = $user['lastName'];
        }        
        
        if(isset($fields['username'])){
            $this->username = $fields['username'];
        }else{
            $this->username = $user['username'];
        }

        if(isset($fields['password'])){
            $hash = md5($salt . $fields['password']);            
            $this->password = $hash;
            $this->salt = $salt;
        }else{
            $this->password = $user['password'];
            $this->salt = $user['salt'];
        }

        if(isset($fields['accessToken'])){
            $this->accessToken = $fields['accessToken'];
        }else{
            $this->accessToken = $user['accessToken'];
        }

        if(isset($fields['email'])){
            $this->email = $fields['email'];
        }else{
            $this->email = $user['email'];
        }

        $update = 'UPDATE user SET firstName = :firstName, lastName = :lastName, username = :username, password = :password, salt = :salt, accessToken = :accessToken, email = :email WHERE id = :userId';
        $data = array(
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'username' => $this->username,
            'password' => $this->password,
            'salt' => $this->salt,
            'accessToken' => $this->accessToken,
            'email' => $this->email,
            'userId' => $userId
        );

        $this->conn->executeUpdate($update , $data);
    }

    public function getUsersBy($fields = array('id' => '', 'username' => '', 'password' => '', 'accessToken' => '', 'role' => ''))
    {
        $sql = 'SELECT * FROM user ';
        $data = array();
        
        if(!empty($fields['password']) && !empty($fields['username'])){
            $sqlChild = 'SELECT salt FROM user WHERE username = "'.$fields['username'].'"';
            $users = $this->conn->fetchAll($sqlChild);
            if(!empty($users)){
                $salt = $users[0]['salt'];
                $hash = md5($salt . $fields['password']);
                $fields['password'] = $hash;
            }else{
                return array();
            }
        }

        if(array_filter($fields)){
            $sql .= 'WHERE';
        }

        foreach($fields as $fieldName => $field){
            if(!empty($field)){
                $split = explode(" ", $sql);
                $lastWord = array_pop($split);
                if($lastWord != 'WHERE'){
                    $sql .= ' AND ';
                }
                $sql .= ' '.$fieldName.' = :'.$fieldName;
                $data[$fieldName] = $field;
            }
        }

        $users = $this->conn->fetchAll($sql , $data);

        return $users;
    }

    public function isUniqueUsername($username)
    {
        $sql = 'SELECT * FROM user WHERE username = :username';
        $data = array('username' => $username);

        $user = $this->conn->fetchAll($sql , $data);

        if(!empty($user)){
            return false;
        }else{
            return true;
        }
    }

    private function saveUser()
    {
        $this->conn->insert('user', array(
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'username' => $this->username,
            'password' => $this->password,
            'salt' => $this->salt,
            'accessToken' => $this->accessToken,
            'email' => $this->email
        ));
    }

    public function deleteUser($userId)
    {
        $sql = 'DELETE FROM user WHERE id = '.$userId.'';
        $query = $this->conn->exec($sql);
    }

    public function getUserFiles($user)
    {
        $sql = 'SELECT * FROM file WHERE user_id = :userId';
        $data = array('userId' => $user['id']);

        $files = $this->conn->fetchAll($sql , $data);

        return $files;
    }

    public function authenticateUser($username, $password)
    {
        $sql = 'SELECT * FROM user WHERE username = :username';
        $user = $this->conn->fetchAssoc($sql, array('username' => $username));

        if($user){
            $salt = $user['salt'];
            $hashedPssword = md5($salt . $password);

            if($user['password'] === $hashedPssword){
                return true;
            }
        }

        return false;
    }

    public function getUserByUsername($username)
    {
        $stmt = $this->conn->executeQuery('SELECT * FROM user WHERE username = ?', array(strtolower($username)));
        if (!$user = $stmt->fetch()) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }
    
    public function setAccessToken($user)
    {
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = 
             substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
        ;// "}"
        $this->accessToken = $uuid;
        
        $this->updateUser($user['id'], array('accessToken' => $this->accessToken));        
    }
    
    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;        
    }
    
}
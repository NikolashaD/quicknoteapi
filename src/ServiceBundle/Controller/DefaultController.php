<?php
namespace ServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Silex\Application;
use Silex\ControllerProviderInterface;

/**
* The routes used for service part.
*
* @package ServiceBundle
*/
class DefaultController
{
	public static function getAccessToken($request)
    {
        return $request->headers->get('accessToken');
    }

    public static function getSecretToken($request)
    {
        return $request->headers->get('secretToken');
    }

    public static function isSecretTokenMatch($secretToken)
    {
        if($secretToken === SECRET_API_TOKEN){
            return true;
        }
        return true;
    }
}
<?php
namespace ServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Silex\Application;
use Silex\ControllerProviderInterface;
use ServiceBundle\Controller\DefaultController;
use ServiceBundle\Model\Note;
use ServiceBundle\Model\User;

/**
* The routes used for service part.
*
* @package ServiceBundle
*/
class NoteController
{
    public function index()
    {
        var_dump('asdf');
    }


    public function create(Request $request,  Application $app)
    {
        $response_data_arr = array('message' => '');
        $status_code = 200;

        $accessToken = DefaultController::getAccessToken($request);
        $secretToken = DefaultController::getSecretToken($request);

        if(DefaultController::isSecretTokenMatch($secretToken)){
            $dataArray = array();

            $title = $request->request->get('title');
            $content = $request->request->get('content');
            $accessToken = $request->getSession()->get('accessToken');

            $userObj = new User($app);
            $users = $userObj->getUsersBy(array('accessToken' => $accessToken));
            if(!empty($users)){
                $userId = $users[0]['id'];
                $noteObj = new Note($app, $title, $content);
                $noteObj->createNote($userId);
                
                $response_data_arr['message'] = 'Success';
            }else{
                $status_code = 400;
                $response_data_arr['message'] = 'Sorry, you should be logged in.';
            }       
        }else{
            $status_code = 400;
            $response_data_arr['message'] = 'Sorry, secret token doesn\'t match';
        }

        $response_data = json_encode($response_data_arr);
        $response = new Response($response_data, $status_code, array('Content-Type' => 'application/json'));
        return $response; 		
    }
    
    public function get(Request $request, Application $app)
    {
        $response_data_arr = array('message' => '');
        $status_code = 200;

        $accessToken = DefaultController::getAccessToken($request);
        $secretToken = DefaultController::getSecretToken($request);
        
        if(DefaultController::isSecretTokenMatch($secretToken)){
            $dataArray = array();        
            if($request->getSession()->get('accessToken')){
                $accessToken = $request->getSession()->get('accessToken');
                $dataArray = array();
                
                $userObj = new User($app);
                $users = $userObj->getUsersBy(array('accessToken' => $accessToken));
                $userId = $users[0]['id'];                

                $noteObj = new Note($app);
                $response_data_arr['notes'] = $noteObj->getNotesByUser($userId);
                $response_data_arr['message'] = 'Success';                
            }else{
                $status_code = 400;
                $response_data_arr['message'] = 'Sorry, you shoul be logged in';
            }
        }else{
            $status_code = 400;
            $response_data_arr['message'] = 'Sorry, secret token doesn\'t match';
        }            
        
        $response_data = json_encode($response_data_arr);
        $response = new Response($response_data, $status_code, array('Content-Type' => 'application/json'));
        return $response;         
    }    
    
    public function delete($id = null, Request $request, Application $app)
    {       
        $response_data_arr = array('message' => '');
        $status_code = 200;

        $accessToken = DefaultController::getAccessToken($request);
        $secretToken = DefaultController::getSecretToken($request);
        
        if(DefaultController::isSecretTokenMatch($secretToken)){
            $dataArray = array();        
            if($request->getSession()->get('accessToken')){
                $noteObj = new Note($app);
                $noteObj->deleteNote($id);
                $response_data_arr['message'] = 'Success';                
            }else{
                $status_code = 400;
                $response_data_arr['message'] = 'Sorry, you shoul be logged in';
            }
        }else{
            $status_code = 400;
            $response_data_arr['message'] = 'Sorry, secret token doesn\'t match';
        }          
        
        $response_data = json_encode($response_data_arr);
        $response = new Response($response_data, $status_code, array('Content-Type' => 'application/json'));
        return $response;   
    }    
 
}
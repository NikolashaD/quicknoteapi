<?php
namespace ServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Silex\Application;
use Silex\ControllerProviderInterface;
use ServiceBundle\Controller\DefaultController;
use ServiceBundle\Model\User;

/**
* The routes used for service part.
*
* @package ServiceBundle
*/
class UserController
{
    public function index()
    {
        var_dump('asdf');
    }


    public function registration(Request $request,  Application $app)
    {
        $response_data_arr = array('message' => '');
        $status_code = 200;

        $accessToken = DefaultController::getAccessToken($request);
        $secretToken = DefaultController::getSecretToken($request);

        if(DefaultController::isSecretTokenMatch($secretToken)){
            $dataArray = array();

            $fName = $request->request->get('firstName');
            $lName = $request->request->get('lastName');
            $uName = $request->request->get('userName');
            $passwd = $request->request->get('password');
            $email = $request->request->get('email');

            $userObj = new User($app);
            $users = $userObj->getUsersBy(array('username' => $uName));
            
            if(empty($users)){
                $userObj->addUser($fName, $lName, $uName, $passwd, $email);
                $response_data_arr['message'] = 'Success';
            }else{
                $status_code = 400;
                $response_data_arr['message'] = 'Sorry, user with such username already exists. Please, choose another one.';
            }       
        }else{
            $status_code = 400;
            $response_data_arr['message'] = 'Sorry, secret token doesn\'t match';
        }

        $response_data = json_encode($response_data_arr);
        $response = new Response($response_data, $status_code, array('Content-Type' => 'application/json'));
        return $response; 		
    }
    
    public function login(Request $request,  Application $app)
    {
        $response_data_arr = array('message' => '', 'data' => array());
        $status_code = 200;

        $accessToken = DefaultController::getAccessToken($request);
        $secretToken = DefaultController::getSecretToken($request);

        if(DefaultController::isSecretTokenMatch($secretToken)){
            $dataArray = array();

            $uName = $request->request->get('userName');
            $passwd = $request->request->get('password');

            $userObj = new User($app);
            $isAuthenticate = $userObj->authenticateUser($uName, $passwd);            
            if($isAuthenticate){
                $user = $userObj->getUserByUsername($uName);
                $userObj->setAccessToken($user);                
                
                $response_data_arr['data'] = $userObj->getUserByUsername($uName);
                $response_data_arr['message'] = 'Welcome to the Quick Note! Enjoy.';
                $request->getSession()->set('accessToken', $response_data_arr['data']['accessToken']);                  
            }else{
                $status_code = 404;
                $response_data_arr['message'] = 'Sorry, password or username is incorrect. Please, try again.';
            }       
        }else{
            $status_code = 400;
            $response_data_arr['message'] = 'Sorry, secret token doesn\'t match';
        }

        $response_data = json_encode($response_data_arr);
        $response = new Response($response_data, $status_code, array('Content-Type' => 'application/json'));
        return $response; 		
    }    
}
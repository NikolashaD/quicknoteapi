<?php
namespace ServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Silex\Application;
use Silex\ControllerProviderInterface;

/**
* The routes used for service part.
*
* @package ServiceBundle
*/
class RoutesController implements ControllerProviderInterface
{
	/**
	* Connect function is used by Silex to mount the controller to the application.
	*
	* Please list all routes inside here.
	*
	* @param Application $app Silex Application Object.
	*
	* @return Response Silex Response Object.
	*/
	public function connect(Application $app)
	{
		/**
		* @var \Silex\ControllerCollection $factory
		*/
		$factory = $app['controllers_factory'];

		$factory->get(
			'/',
			'\ServiceBundle\Controller\UserController::index'
		);     

		$factory->post(
			'/registration',
			'\ServiceBundle\Controller\UserController::registration'
		);   
		$factory->post(
			'/login',
			'\ServiceBundle\Controller\UserController::login'
		);   
		$factory->post(
			'/note/create',
			'\ServiceBundle\Controller\NoteController::create'
		);        
		$factory->get(
			'/notes',
			'\ServiceBundle\Controller\NoteController::get'
		);   
		$factory->get(
			'/note/delete/{id}',
			'\ServiceBundle\Controller\NoteController::delete'
		);   
		$factory->post(
			'/search',
			'\ServiceBundle\Controller\SearchController::search'
		);                  

		return $factory;
	}
}
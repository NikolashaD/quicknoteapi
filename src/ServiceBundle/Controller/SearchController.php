<?php
namespace ServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Silex\Application;
use Silex\ControllerProviderInterface;
use ServiceBundle\Controller\DefaultController;
use ServiceBundle\Model\User;
use ServiceBundle\Model\Note;
use SphinxClient;

/**
* The routes used for service part.
*
* @package ServiceBundle
*/
class SearchController
{
    public function index()
    {
        var_dump('asdf');
    }


    public function search(Request $request,  Application $app)
    {
        $response_data_arr = array('message' => '');
        $status_code = 200;

        $accessToken = DefaultController::getAccessToken($request);
        $secretToken = DefaultController::getSecretToken($request);

        if(DefaultController::isSecretTokenMatch($secretToken)){
            $dataArray = array();

            $text = $request->request->get('text');
            
            $cl = new SphinxClient();
            $cl->SetServer( "localhost", 9312 );


            $cl->SetMatchMode( SPH_MATCH_ANY  ); 
            $result = $cl->Query($text); 

            if(!empty($result["matches"])){ 
                foreach($result["matches"] as $product => $info){
                      $dataArray[$product] = $info;
                }               
            }
            $response_data_arr['result'] = $dataArray;
            $response_data_arr['message'] = 'Success'; 
        }else{
            $status_code = 400;
            $response_data_arr['message'] = 'Sorry, secret token doesn\'t match';
        }

        $response_data = json_encode($response_data_arr);
        $response = new Response($response_data, $status_code, array('Content-Type' => 'application/json'));
        return $response; 		
    }
    
    
}